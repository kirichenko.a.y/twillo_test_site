// Import our custom CSS
import '../scss/styles.scss'

// Import all of Bootstrap's JS
import * as bootstrap from 'bootstrap'

window.addEventListener('scroll', () => {
    const fullPageSection = document.querySelector('.full-page-section');
    const scrollPosition = fullPageSection.getBoundingClientRect().top;
    
    if (!window.matchMedia("(max-width: 960px)").matches){
        if (scrollPosition === 0 || scrollPosition < 0) {
        fullPageSection.classList.add('scroll-snap');
        } else {
            fullPageSection.classList.remove('scroll-snap');
        }
    }
});
